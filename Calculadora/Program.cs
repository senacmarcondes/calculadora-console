﻿using System;

namespace Calculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = 0; double num2 = 0;

            Console.WriteLine("Calculadora\r");
            
            Console.WriteLine("------------------------\n");

            Console.WriteLine("Digite um numero:");
            num1 = Convert.ToDouble(Console.ReadLine());


            Console.WriteLine("Digite um numero:");
            num2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Escolha uma opção da seguinte lista:");
            Console.WriteLine("\tA - Soma");
            Console.WriteLine("\tS - Subtração");
            Console.WriteLine("\tM - Multiplicação");
            Console.WriteLine("\tD - Divisão");
            Console.Write("Sua opção? ");


            switch (Console.ReadLine())
            {
                case "a":
                    Console.WriteLine($"Seu resultado: {num1} + {num2} = " + (num1 + num2));
                    break;
                case "s":
                    Console.WriteLine($"Seu resultado: {num1} - {num2} = " + (num1 - num2));
                    break;
                case "m":
                    Console.WriteLine($"Seu resultado: {num1} * {num2} = " + (num1 * num2));
                    break;
                case "d":
                    Console.WriteLine($"Seu resultado: {num1} / {num2} = " + (num1 / num2));
                    break;
            }
            Console.ReadKey();
        }
    }
}